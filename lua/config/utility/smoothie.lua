-- Smooth scrolling
local g = vim.g

g.smoothie_enable = true
g.smoothie_no_default_mappings = false
g.smoothie_experimental_mappings = true
