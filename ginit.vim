""" NVim QT configuration
packadd neovim-gui-shim
GuiTabline 0
GuiScrollBar 0
GuiRenderLigatures 0
GuiLinespace 1
Guifont! SauceCodePro Nerd Font Mono:h10
